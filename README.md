# What is this project

Student number: e1800951
Name: Phuc Le

This project is my final project for the class H2C2101-3002 Introduction to Programming by Mr Timo.

I know by March 2022, I will be really busy and do not have enough time to do, so I prepared this in January 2022. This project is use to generate lowfi music and ammbient sound while you are working. By doing this project, I am able to learn a lot of new things as well as styling technique.

# List of requirement and self check

. Should include several (>3) components: Checked
. Should use props to pass data between the components: Checked
. Should be structured correctly (components, containers, menu/Menu.jsx, menu/Menu.css): Checked
. Should be implemented in modern favor: const MyApp=(props)=>: Checked
. Should only use functional component and hooks: Checked
. Should have unit test (>3) to test components: Have not done yet
. Should fetch some data from RESTful backend (like Firebase): Have not done yet, so far I only use firebase for authentification and deploy, I have the intention of using firebase to saving all of my user data, but I do not have time yet since I also have to do my thesis and internship full time.
. Source code should be in gitlab.com: Checked
. Should run by npm i, npm start, npm run build, firebase deploy: Checked
. Should have some menu navigation: Have the context menu on the right as well as the navigation bar on top
. Should have some input fields: Checked
. The app should be deployd (to Firebase, public_html,...): Deploy to netlify and Firebase, reason: when I completed my study my public_html will be deleted so I do not want to deploy there.

# Image

![image](https://user-images.githubusercontent.com/47014132/149120770-381c8ea9-9d57-40b7-bd10-3d39d4dce42a.png)
![image](https://user-images.githubusercontent.com/47014132/149120793-0c75511e-88f4-4e69-b836-dabd1eeac884.png)

# Demo

[Demo](https://lofiphucle.netlify.app/)

## How to build and run

### 1. Install NodeJs and npm

Download and install Node.js from this link https://nodejs.org/en/download/

### 2. Clone or download this repo.

On your terminal or cmd, type: `git clone git@github.com:phuclevinh2000/Lofi-website.git`

. On your command line, navigate to root folder and install: `npm install`

### 3. Deploy the application

. Deploy the app on web-browsers: `npm start`

### Technology use in the application

. React
. Redux
. Firebase
. ReactPlayer
. Google Authentification
. SASS
